/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.memory;

import java.nio.charset.StandardCharsets;
import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.util.VMSupport;

/**
 *
 */
public class StringSize {
  public static void main(String[] args) throws Exception {
    System.out.println(VMSupport.vmDetails());
    System.out.println(ClassLayout.parseClass(String.class).toPrintable());
    System.out.println(ClassLayout.parseClass(byte[].class).toPrintable());
    System.out.println(ClassLayout.parseInstance("ABCDEFGHIJ").toPrintable());
    System.out.println(ClassLayout.parseInstance("ABCDEFGHIJ".toCharArray()).toPrintable());
    System.out.println(ClassLayout.parseInstance("ABCDEFGHIJ".getBytes()).toPrintable());
    System.out.println(ClassLayout.parseInstance("ABCDEFGHIJ".getBytes(StandardCharsets.UTF_16)).toPrintable());
  }
}
