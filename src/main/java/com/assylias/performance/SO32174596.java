/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.stream.IntStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO32174596 {
  @Benchmark public int intStream_empty() {
    IntStream.range(0, 1000000000).forEach(i -> {
    });
    return 0;
  }

  @Benchmark public void intStream_consume(Blackhole bh) {
    IntStream.range(0, 1000000000).forEach(bh::consume);
  }

  @Benchmark public void nestedIntStream_empty(Blackhole bh) {
    IntStream.range(0, 1000000000).forEach(i -> {
      IntStream.range(0, 1).forEach(j -> {
      });
    });
  }

  @Benchmark public void nestedIntStream_consume(Blackhole bh) {
    IntStream.range(0, 1000000000).forEach(i -> {
      IntStream.range(0, 1).forEach(bh::consume);
    });
  }
}
