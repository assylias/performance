/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import static java.util.stream.Collectors.toList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class CodeFxStream {

  int[] intArray = new Random().ints(500_000).toArray();
  List<Integer> intList = new Random().ints(500_000).boxed().collect(toList());

  @Benchmark
  public int array_max_for() {
    int m = Integer.MIN_VALUE;
    for (int i = 0; i < intArray.length; i++) {
      if (intArray[i] > m)
        m = intArray[i];
    }
    return m;
  }

  @Benchmark
  public int array_max_stream() {
    return Arrays.stream(intArray).max().getAsInt();
  }

  @Benchmark
  public int list_max_for() {
    int m = Integer.MIN_VALUE;
    for (int i = 0; i < intList.size(); i++) {
      if (intList.get(i) > m)
        m = intList.get(i);
    }
    return m;
  }

  @Benchmark
  public int list_max_stream() {
    return intList.stream().max(Math::max).get();
  }

  @Benchmark
  public int list_max_stream_unboxed() {
    return intList.stream().mapToInt(x -> x).max().getAsInt();
  }
}
