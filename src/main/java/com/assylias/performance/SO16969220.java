/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Random;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO16969220 {

  private int i;

  @Setup public void randomise() {
    Random r = new Random();
    i = r.nextInt();
  }

  @Benchmark public boolean isEvenBit() {
    return (i & 1) == 0;
  }

  @Benchmark public boolean isEvenMod() {
    return i % 2 == 0;
  }
}
