/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import static java.util.stream.IntStream.range;
import java.util.stream.Stream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO35037893 {

  static Random random = new Random();

  @Param({"100"}) int size;

  double[][] a1;
  double[][] a2;

  @Setup(Level.Invocation) public void setup() {
    a1 = getNxNMatrix(size);
    a2 = getNxNMatrix(size);
  }

  private static double[][] getNxNMatrix(int n) {
    return IntStream.range(0, n)
            .mapToObj(r -> random.doubles(n, 0, 10).toArray())
            .toArray(double[][]::new);
  }

  @Benchmark public double[][] loop() {
    int n = a1.length;
    int m = a2.length;
    int p = a2[0].length;

    double[][] result = new double[n][p];

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < p; j++) {
        for (int k = 0; k < m; k++) {
          result[i][j] += a1[i][k] * a2[k][j];
        }
      }
    }
    return result;
  }

  @Benchmark public double[][] stream() {
    Stream<double[]> stream = Arrays.stream(a1);
    return stream.map(r -> range(0, a2[0].length)
            .mapToDouble(j -> range(0, a2.length)
                    .mapToDouble(k -> r[k] * a2[k][j])
                    .sum()
            )
            .toArray())
            .toArray(double[][]::new);
  }

  @Benchmark public double[][] stream_manual_sum() {
    int m = a2.length;
    int p = a2[0].length;
//    double[][] a = a2;

    Stream<double[]> stream = Arrays.stream(a1);
    return stream.map(r -> range(0, p)
            .mapToDouble(j -> sum(range(0, m)
                    .mapToDouble(k -> r[k] * a2[k][j]))
            )
            .toArray())
            .toArray(double[][]::new);
  }

  private static double sum(DoubleStream ds) {
    double[] sum = new double[1];
    ds.forEach(d -> sum[0] += d);
    return sum[0];
  }

  /**
   * Benchmark       (size)  Mode  Cnt      Score     Error   Units
   * SO35037893.loop    100  avgt   10   1448.807   ± 59.460  us/op
   * SO35037893.stream  100  avgt   10  12035.692  ± 528.210  us/op
   */
}
