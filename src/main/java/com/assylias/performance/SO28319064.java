/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO28319064 {
  private int n = 1_000_000;
  private final List<String> csv = new ArrayList<>();

  @Setup public void populateList() {
    Random r = new Random();
    for (int i = 0; i < n; i++) {
      String line = r.ints(5)
                            .mapToObj(String::valueOf)
                            .collect(joining(","));
      csv.add(i % 5 == 0 ? null : line);
    }
  }

  @Benchmark public List<String> forEach() {
    List<String> result = new ArrayList<> ();
    csv.stream()
            .filter(e -> e != null)
            .forEach(s -> result.add(parse(s)));
    return result;
  }

  @Benchmark public List<String> map() {
    return csv.stream()
                   .filter(Objects::nonNull)
                   .map(this::parse)
                   .collect(toList());
  }

  private String parse(String line) {
    return line.split(",")[0];
  }
}
