/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO35037893_sum {

  static Random random = new Random();

  @Param({"1000"}) int size;

  double[] array;

  @Setup(Level.Invocation) public void setup() {
    array = random.doubles(size, 0, 10).toArray();
  }

  @Benchmark public double loop() {
    double sum = 0;
    for (int i = 0; i < size; i++) {
      sum += array[i];
    }
    return sum;
  }

  @Benchmark public double stream() {
    return Arrays.stream(array).sum();
  }

  @Benchmark public double stream_manual() {
    return sum(Arrays.stream(array));
  }

  private static double sum(DoubleStream ds) {
    double[] sum = new double[1];
    ds.forEach(d -> sum[0] += d);
    return sum[0];
  }
}
