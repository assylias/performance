/*
 * Copyright 2014 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assylias.performance;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;


@OutputTimeUnit(TimeUnit.MICROSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO21968918 {

    public int digitSum(BigInteger x) {
        int sum = 0;
        for (char c : x.toString().toCharArray()) {
            sum += Integer.valueOf(c + "");
        }
        return sum;
    }

    @Benchmark public int solve() {
        int max = 0;
        for (int i = 1; i < 100; i++) {
            for (int j = 1; j < 100; j++) {
                max = Math.max(max, digitSum(BigInteger.valueOf(i).pow(j)));
            }
        }
        return max;
    }

    @Benchmark public int solveLambda() {
        return  IntStream.range(1, 100)
                .map(i -> IntStream.range(1, 100).map(j -> digitSum(BigInteger.valueOf(i).pow(j))).max().getAsInt())
                .max().getAsInt();
    }

    @Benchmark public int solveLambdaParallel() {
        return  IntStream.range(1, 100)
                .parallel()
                .map(i -> IntStream.range(1, 100).map(j -> digitSum(BigInteger.valueOf(i).pow(j))).max().getAsInt())
                .max().getAsInt();
    }
}
