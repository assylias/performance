/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * http://stackoverflow.com/questions/17379020/how-to-find-the-largest-power-of-2-less-than-the-given-number
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO17379020 {

    private static final double LOG_2 = Math.log(2);
    private static final Random rand = new Random();
    private final int max = 1 << 30;
    private final int offset = 0;
    private int veryRandom = 12345; //nice ini't?

    @Benchmark
    public int baseline() {
        int n = getN();
        return n;
    }

    @Benchmark
    public int bitShift() {
        int n = getN();
        int result = 1;
        while (result < n)
            result <<= 1;
        return result >> 1;
    }

    @Benchmark
    public int manualBitShift() {
        int n = getN();
        n--;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        n++;
        return n >> 1;
    }

    @Benchmark
    public int log() {
        int n = getN();
        return 1 << (int) (Math.log(n-1) / LOG_2);
    }

    private int getN() {
        int n = veryRandom;//rand.nextInt(max) + offset;
        return n;
    }
}
