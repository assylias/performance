/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import org.openjdk.jmh.Main;

public class RunTest {

    private static final String TEST = ".*SO22923505.*";

    public static void main(String[] args) throws Exception {
        Main.main(getArguments(TEST, 5, 2000, 1));
    }

    private static String[] getArguments(String className, int nRuns, int runForMilliseconds, int nThreads) {
        return new String[]{className,
            "-i", "" + nRuns,
            "-r", runForMilliseconds + "ms",
            "-t", "" + nThreads,
            "-w", "1000ms",
            "-wi", "5",
            "-f", "1",
            "-v", "NORMAL"
//            "--jvmargs", "-XX:+UnlockDiagnosticVMOptions -XX:CompileCommand=print,*18667440.slowItDown*"
        };
    }
}
