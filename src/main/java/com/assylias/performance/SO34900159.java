/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import java.util.HashMap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO34900159 {

  HashMap<String,String> hashmapVar = new HashMap<String,String>();
  String key0 = "bye";

  @Setup(Level.Invocation)
  public void setup(){
//      hashmapVar.put("bye","");
  }

  @Benchmark
  public void hashmapGet(Blackhole bh) {
      bh.consume(hashmapVar.get(key0));
  }

  @Benchmark
  public void hashmapRemove(Blackhole bh) {
      bh.consume(hashmapVar.remove(key0));
  }
}
