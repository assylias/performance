/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO29253040 {

  char c = 'A';

  @Benchmark public String concat() {
    return "" + c;
  }

  @Benchmark public String valueOf() {
    return String.valueOf(c);
  }

  @Benchmark public String toString() {
    int i = 0;
    class A {
      public int m() {
        return i + 1;
      }
    }
    return Character.toString(c);
  }

}
