/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO30454904 {

  ProblemFive instance = new ProblemFive();

  static class ProblemFive {

    int twentyDivCount(int a) {
      int count = 0;
      for (int i = 1; i < 21; i++) {

        if (a % i == 0) {
          count++;
        }
      }
      return count;
    }

    static int twentyDivCountStatic(int a) {
      int count = 0;
      for (int i = 1; i < 21; i++) {

        if (a % i == 0) {
          count++;
        }
      }
      return count;
    }
  }
  static int i = 0;

  @Setup public void setup() {
    i++;
  }

  @Benchmark public int instanceMethod() {
    return instance.twentyDivCount(i);
  }

  @Benchmark public int staticMethod() {
    return ProblemFive.twentyDivCountStatic(i);
  }
}
