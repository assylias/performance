/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.IntToLongFunction;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO34929316 {

  static final IntToLongFunction OP = n -> n * n * 11 + n * 7;

  @Param({"10", "1000", "100000", "1000000", "10000000"}) int size;
  static int procs = Runtime.getRuntime().availableProcessors() + 1;
  ExecutorService es = Executors.newFixedThreadPool(procs);
  long[] array;

  @Setup(Level.Iteration)
  public void setup() {
    array = new long[size];
    es = Executors.newFixedThreadPool(procs);
  }

  @TearDown(Level.Iteration)
  public void tearDown() throws Exception {
    es.shutdown();
    if (!es.awaitTermination(1, TimeUnit.SECONDS)) {
      System.out.println("DID NOT SHUTDOWN");
    }
  }

  @Benchmark
  public long setAll() {
    Arrays.setAll(array, OP);
    return array[0];
  }

  @Benchmark
  public long parallelSetAll() {
    Arrays.parallelSetAll(array, OP);
    return array[0];
  }

  @Benchmark
  public long customSetAll() throws Exception {
    if (size > 1) {
      int batch = size / procs;
      Future<?>[] ff = new Future<?>[procs];
      for (int i = 0; i < procs; i++) {
        ff[i] = es.submit(set(array, batch * i, batch * (i + 1)));
      }
      for (Future<?> f : ff) f.get();
    } else {
      array[0] = 0;
    }
    return array[0];
  }

  private Runnable set(long[] array, int from, int to) {
    return () -> {
      int adjustedTo = Math.min(to, array.length);
      for (int i = from; i < adjustedTo; i++) array[i] = OP.applyAsLong(i);
    };
  }
}
