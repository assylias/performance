/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;

/**
 *
 * @author Yann Le Tallec
 */
@BenchmarkMode(Mode.AverageTime)
public class ListsComparison {
    
    private static final int SIZE = 10000;

    @Benchmark
    public int list() {
        Integer[] array = getArray(SIZE);
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-1));

        linearInsertionAsList(array, list);

        return list.size();
    }

    @Benchmark
    public int arraylist() {
        Integer[] array = getArray(SIZE);
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-1));

        linearInsertionAsArrayList(array, list);

        return list.size();
    }

    private Integer[] getArray(int size) {
        Integer[] array = new Integer[size];
        Random random = new Random(123456789L);
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(Integer.MAX_VALUE);
        }
        return array;
    }

    private static void linearInsertionAsList(Integer[] intArray, List<Integer> list) {
        for (Integer integer : intArray) {
            int list_size = list.size(); // avoid calculating the size every time
            for (int i = 0; i < list_size; i++) {
                if (integer.compareTo(list.get(i)) >= 0) {
                    list.add(i, integer);
                    break;
                }
            }
        }
    }

    private static void linearInsertionAsArrayList(Integer[] intArray, ArrayList<Integer> list) {
        for (Integer integer : intArray) {
            int list_size = list.size(); // avoid calculating the size every time
            for (int i = 0; i < list_size; i++) {
                if (integer.compareTo(list.get(i)) >= 0) {
                    list.add(i, integer);
                    break;
                }
            }
        }
    }
}
