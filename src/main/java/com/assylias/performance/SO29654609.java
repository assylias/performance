/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO29654609 {

  int[] nums = {1, 2, 3, 4, 5, 6};

  @Benchmark public void printf(Blackhole blackHole) {
    redirectSystemOutTo(blackHole);
    System.out.printf("%d %d %d %d %d %d\n", nums[0], nums[1], nums[2], nums[3], nums[4], nums[5]);
  }

  @Benchmark public void builder(Blackhole blackHole) {
    redirectSystemOutTo(blackHole);
    StringBuilder sb = new StringBuilder();
    sb.append(nums[0]);
    sb.append(' ');
    sb.append(nums[1]);
    sb.append(' ');
    sb.append(nums[2]);
    sb.append(' ');
    sb.append(nums[3]);
    sb.append(' ');
    sb.append(nums[4]);
    sb.append(' ');
    sb.append(nums[5]);
    System.out.println(sb.toString());
  }

  private void redirectSystemOutTo(Blackhole blackHole) {
    OutputStream os = new OutputStream() {
      @Override public void write(int b) throws IOException {
        blackHole.consume(b);
      }
    };
    System.setOut(new PrintStream(os));
  }

}
