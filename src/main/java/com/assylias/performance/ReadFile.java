/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import com.google.common.base.Charsets;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 * Compare various methods to read lines from a file and output the content as an array
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class ReadFile {

//    private static final Path path = Paths.get("./src/res/short.txt");
//    private static final Path path = Paths.get("./src/res/long.txt");
    private static final Path path = Paths.get("./src/res/LesMiserables.txt");
    private static final File file = path.toFile();

    @Benchmark
    public String[] method1() throws Exception {
        Scanner in = new Scanner(file);

        int counter = 0;

        while (in.hasNextLine()) {
            in.nextLine();
            counter++;
        }

        String[] data = new String[counter];

        in = new Scanner(file);

        int i = 0;

        while (in.hasNextLine()) {
            data[i] = in.nextLine();
            i++;
        }

        return data;
    }

    @Benchmark
    public String[] method2() throws Exception {
        List<String> lines = Files.readAllLines(path, Charsets.UTF_8);
        return lines.toArray(new String[lines.size()]);
    }

    @Benchmark
    public String[] method3() throws Exception {
        FileReader reader = new FileReader(file);

        String content = null;

        char[] chars = new char[(int) file.length()];
        reader.read(chars);
        content = new String(chars);

        String[] data = content.split(System.getProperty("line.separator"));

        reader.close();
        
        return data;
    }
    
    @Benchmark
    public String[] method4() throws Exception {
        byte[] bytes = Files.readAllBytes(path);
        String s = new String(bytes, Charsets.UTF_8);
        return s.split(System.getProperty("line.separator"));
    }
}
