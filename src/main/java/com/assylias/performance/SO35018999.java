/*
 * Copyright (C) 2016 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO35018999 {

  private static final int LIST_LENGTH = 1000 * 1000;

  Random r;
  List<Double> unsorted;
  List<Double> shuffled;
  List<Double> sorted;
  List<Double> sortedContiguous;

  double valueAtIndex_sorted;
  double valueAtIndex_sortedContiguous;
  double valueAtIndex_unsorted;
  double valueAtIndex_shuffled;

  @Setup(Level.Iteration)
  public void iterationSetup() {
    r = new Random();
    unsorted = new ArrayList<>(LIST_LENGTH);
    shuffled = new ArrayList<>(LIST_LENGTH);
    sorted = new ArrayList<>(LIST_LENGTH);
    sortedContiguous = new ArrayList<>(LIST_LENGTH);
    for (int i = 0; i < LIST_LENGTH; i++) {
      unsorted.add(r.nextDouble());
      sorted.add(r.nextDouble());
      shuffled.add(r.nextDouble());
    }
    Collections.sort(sorted);
    Collections.shuffle(shuffled);
    for (int i = 0; i < LIST_LENGTH; i++) {
      sortedContiguous.add((double) sorted.get(i));
    }
  }

  @Setup(Level.Invocation)
  public void invocationSetup() {
    int index = r.nextInt(LIST_LENGTH);
    valueAtIndex_sorted = sorted.get(index);
    valueAtIndex_sortedContiguous = sorted.get(index);
    valueAtIndex_unsorted = unsorted.get(index);
    valueAtIndex_shuffled = shuffled.get(index);
  }

  @Benchmark
  public int unsorted() {
    return unsorted.indexOf(valueAtIndex_unsorted);
  }

  @Benchmark
  public int shuffled() {
    return shuffled.indexOf(valueAtIndex_shuffled);
  }

  @Benchmark
  public int sorted() {
    return sorted.indexOf(valueAtIndex_sorted);
  }

  @Benchmark
  public int sorted_contiguous() {
    return sortedContiguous.indexOf(valueAtIndex_sortedContiguous);
  }



  /**
   * Benchmark                     Mode  Cnt  Score   Error  Units
   * SO35018999.shuffled           avgt   10  8.895 ± 1.534  ms/op
   * SO35018999.sorted             avgt   10  8.093 ± 3.093  ms/op
   * SO35018999.sorted_contiguous  avgt   10  1.665 ± 0.397  ms/op
   * SO35018999.unsorted           avgt   10  2.700 ± 0.302  ms/op
   */
}
