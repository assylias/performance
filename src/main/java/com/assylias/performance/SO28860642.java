/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO28860642 {

  @Param({"0"/*, "25", "50"*/}) int n;

  private final A[] array = new A[100];

  public SO28860642() {
    for (int i = 0; i < 100; i++) {
      array[i] = new A(i);
    }
  }

  @Benchmark public A[] oneliner() {
    for (int i = 0; i < array.length; i++) {
      array[i] = array[i].resolve(n);
    }
    return array;
  }

  @Benchmark public A[] condition() {
    for (int i = 0; i < array.length; i++) {
      A t1 = array[i];
      A t2 = t1.resolve(n);
      if (t1 != t2) array[i] = t2;
    }
    return array;
  }

  public static class A {
    private final int value;
    public A(int value) { this.value = value; }

    private A resolve(int threshold) {
      return this;
//      if (value >= threshold) return this;
//      else return new A(value);
    }
  }

}
