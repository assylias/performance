/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import java.util.stream.Stream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    public class SO30940682 {

      @Param({">aaaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaa>", "aaaaaaaaaaaaaaaaaaaaap", "paaaaaaaaaaaaaaaaaaaaa"}) String s;

      @Benchmark public boolean stream() {
        return Stream.of(">", "<", "&", "l", "p").anyMatch(s::contains);
      }

      @Benchmark public boolean regex() {
        return s.matches("^.*?(>|<|&|l|p).*$");
      }
    }
