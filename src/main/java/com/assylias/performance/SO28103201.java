/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO28103201 {
  private static List<Integer> nums;
  @Param({"1000", "100000", "10000000"}) int n;

  @Setup public void setup() {
    nums = new ArrayList<> (n);
    IntStream.range(0, n).forEach(nums::add);
  }

  @Benchmark public int forLoop() {
    int sum = 0;
    for (Integer i : nums) sum += i;
    return sum;
  }

  @Benchmark public int sequential() {
    return nums.stream().reduce(0, Integer::sum);
  }

  @Benchmark public int sequential_unbox() {
    return nums.stream().mapToInt(Integer::intValue).sum();
  }

  @Benchmark public int parallel() {
    return nums.parallelStream().reduce(0, Integer::sum);
  }

  @Benchmark public int parallel_unbox() {
    return nums.parallelStream().mapToInt(Integer::intValue).sum();
  }
}