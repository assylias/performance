/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO30507506 {

  @Param({"1", "12345", "123456789"}) int n;
  int i;
  String s;

  @Setup public void setup() {
    i = n;
    s = String.valueOf(n);
  }

  @Benchmark public boolean parse() {
    return Integer.parseInt(s) == i;
  }

  @Benchmark public boolean stringEquals() {
    return s.equals(Integer.toString(i));
  }

  @Benchmark public boolean manual() {
    return parseManual(s) == i;
  }

  @Benchmark public boolean manualAtlaste() {
    return equalsIntString(i, s);
  }

  @Benchmark public boolean parse_unequal() {
    return Integer.parseInt(s) == i * 2;
  }

  @Benchmark public boolean stringEquals_unequal() {
    return s.equals(Integer.toString(i * 2));
  }

  @Benchmark public boolean manual_unequal() {
    return parseManual(s) == i * 2;
  }

  @Benchmark public boolean manualAtlaste_unequal() {
    return equalsIntString(i * 2, s);
  }

  private static int parseManual(String s) {
    int result = 0;
    int sign = s.charAt(0) == '-' ? -1 : 1;
    int startIndex = (s.charAt(0) >= '0' && s.charAt(0) <= '9') ? 0 : 1;
    for (int i = startIndex; i < s.length(); i++) {
      result *= 10;
      result += s.charAt(i) - '0';
    }
    return result * sign;
  }

  private static boolean equalsIntString(int value, String s) {
    if (s.isEmpty()) return false; // This is never good.
    if ((s.charAt(0) == '-' && value >= 0) || (s.charAt(0) != '-' && value < 0)) return false; // positive/negative check

    // Define the limit. This is basically the end of the string to check.
    int limit = 0;
    if (value < 0) {
      limit = 1;
      value = -value;
    }

    for (int i = s.length() - 1; i >= limit; --i) {
      char expected = (char) ('0' + (value % 10)); // the modulo will be optimized by the JIT because 10 is a constant
      value /= 10; // same story.

      if (s.charAt(i) != expected) return false;
    }
    return true;
  }
}
