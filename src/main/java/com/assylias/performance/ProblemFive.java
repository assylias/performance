/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

/**
 *
 */
public class ProblemFive {
  // counts the number of numbers that the entry is evenly divisible by, as max is 20
  int twentyDivCount(int a) {  // change to static int.... when using it directly
    int count = 0;
    for (int i = 1; i < 21; i++) {

      if (a % i == 0) {
        count++;
      }
    }
    return count;
  }

  public static void main(String[] args) {
    long startT = System.nanoTime();
    int start = 500000000;
    int result = start;

    ProblemFive ff = new ProblemFive();

    for (int i = start; i > 0; i--) {

      int temp = ff.twentyDivCount(i); // faster way
      // twentyDivCount(i) - slower

      if (temp == 20) {
        result = i;
        System.out.println(result);
      }
    }

    System.out.println(result);

    long end = System.nanoTime();
    System.out.println(((end - startT) / 1000000) + " ms");

  }
}
