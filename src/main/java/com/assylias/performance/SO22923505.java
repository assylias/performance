/*
 * Copyright 2014 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assylias.performance;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * What is the best way of removing the min and max from a stream?
 * # Benchmark: com.assylias.performance.SO22923505.maxMin
 *
 * Result : 6996.190 ±(99.9%) 284.287 ns/op
 *   Statistics: (min, avg, max) = (6884.690, 6996.190, 7073.344), stdev = 73.828
 *   Confidence interval (99.9%): [6711.903, 7280.477]
 *
 *
 * # Benchmark: com.assylias.performance.SO22923505.skipLimit
 *
 * Result : 479.935 ±(99.9%) 4.547 ns/op
 *   Statistics: (min, avg, max) = (478.566, 479.935, 481.643), stdev = 1.181
 *   Confidence interval (99.9%): [475.388, 484.483]
 *
 *
 * Benchmark                      Mode   Samples         Mean   Mean error    Units
 * c.a.p.SO22923505.maxMin        avgt         5     6996.190      284.287    ns/op
 * c.a.p.SO22923505.skipLimit     avgt         5      479.935        4.547    ns/op
 *
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO22923505 {

    private Set<Double> scores = new HashSet<>(Arrays.asList(1d, 3d, 4d, 5d, 6d, 7d, 12d, 11d, 8d, 2d));

    @Benchmark
    public double maxMin() {
        return scores.stream()
            .mapToDouble(Double::doubleValue)
            .filter((num) -> { //Exclude min and max score
                return num != scores.stream()
                                    .mapToDouble(Double::doubleValue)
                                    .max().getAsDouble()
                        &&
                       num != scores.stream()
                                    .mapToDouble(Double::doubleValue)
                                    .min().getAsDouble();
            })
            .average().getAsDouble();
    }

    @Benchmark
    public double skipLimit() {
      return scores.stream()
              .mapToDouble(Double::doubleValue)
              .sorted()
              .skip(1)
              .limit(scores.size() - 2)
              .average().getAsDouble();

    }
}
