/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import com.google.common.collect.TreeMultiset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import org.apache.commons.collections4.list.TreeList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO28164665 {
  @Param({"100", "1000", "5000", "10000", "100000"}) int n;
  private int current = 0;

  @Benchmark public int[] baseLine() {
    int[] array = new int[n];

    Random r = new Random();
    for (int i = 0; i < n; i++) {
      current = array[i] = r.nextInt();
    }
    return array;
  }

  @Benchmark public LinkedList<Integer> sortLL() {
    LinkedList<Integer> list = new LinkedList<> ();
    if (n > 10000) return list; //skip bc it's too slow!

    Random r = new Random();
    for (int i = 0; i < n; i++) {
      list.add(r.nextInt());
      Collections.sort(list);
      current = list.getFirst(); //O(1), to make sure the sort is not optimised away
    }
    return list;
  }

  @Benchmark public ArrayList<Integer> sortAL() {
    ArrayList<Integer> list = new ArrayList<> ();
    if (n > 10000) return list; //skip bc it's too slow!

    Random r = new Random();
    for (int i = 0; i < n; i++) {
      list.add(r.nextInt());
      Collections.sort(list);
      current = list.get(0); //O(1), to make sure the sort is not optimised away
    }
    return list;
  }

  @Benchmark public LinkedList<Integer> binarySearchLL() {
    LinkedList<Integer> list = new LinkedList<> ();
    if (n > 10000) return list; //skip bc it's too slow!

    Random r = new Random();
    for (int i = 0; i < n; i++) {
      int num = r.nextInt();
      int index = Collections.binarySearch(list, num);
      if (index >= 0) list.add(index, num);
      else list.add(-index - 1, num);
      current = list.getFirst(); //O(1), to make sure the sort is not optimised away
    }
    return list;
  }

    @Benchmark public ArrayList<Integer> binarySearchAL() {
      ArrayList<Integer> list = new ArrayList<> ();

      Random r = new Random();
      for (int i = 0; i < n; i++) {
        int num = r.nextInt();
        int index = Collections.binarySearch(list, num);
        if (index >= 0) list.add(index, num);
        else list.add(-index - 1, num);
        current = list.get(0); //O(1), to make sure the sort is not optimised away
      }
      return list;
    }

    @Benchmark public TreeMultiset<Integer> treeMultiSet() {
      TreeMultiset<Integer> list = TreeMultiset.create();

      Random r = new Random();
      for (int i = 0; i < n; i++) {
        int num = r.nextInt();
        list.add(num);
        current = list.firstEntry().getElement(); //O(1), to make sure the sort is not optimised away
      }
      return list;
    }

  @Benchmark public TreeList<Integer> binarySearchTreeList() {
    TreeList<Integer> list = new TreeList<> ();
    if (n > 10000) return list; //skip bc it's too slow!

    Random r = new Random();
    for (int i = 0; i < n; i++) {
      int num = r.nextInt();
      int index = Collections.binarySearch(list, num);
      if (index >= 0) list.add(index, num);
      else list.add(-index - 1, num);
      current = list.get(0); //O(1), to make sure the sort is not optimised away
    }
    return list;
  }
}
