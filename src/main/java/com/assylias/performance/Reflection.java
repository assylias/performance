/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class Reflection {

    private final static int SIZE = ThisClass.class.getDeclaredFields().length;

    @Benchmark
    public Map<Integer, String> getDeclaredFields() {
        Map<Integer, String> m = new HashMap<> (ThisClass.class.getDeclaredFields().length, 1.0f);
        for (int i = 0; i < SIZE; i++) {
            m.put(i, "a");
        }
        return m;
    }

    @Benchmark
    public Map<Integer, String> knownSize() {
        Map<Integer, String> m = new HashMap<> (SIZE, 1.0f);
        for (int i = 0; i < SIZE; i++) {
            m.put(i, "a");
        }
        return m;
    }

    @Benchmark
    public Map<Integer, String> resize() {
        Map<Integer, String> m = new HashMap<> (8, 0.75f);
        for (int i = 0; i < SIZE; i++) {
            m.put(i, "a");
        }
        return m;
    }
}

class ThisClass {

    private int a, b, c, d, e, f, g, h, i, j, k, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10;
    public String s, t, u;
    protected List l, m, n;
}