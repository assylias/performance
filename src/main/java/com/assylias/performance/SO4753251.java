/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class SO4753251 {

  @Param({"1", "999999999", "9223372036854775807"}) long n;

  @Benchmark public String formatMap() {
    return formatMap(n);
  }

  @Benchmark public String formatArray() {
    return formatArray(n);
  }

  @Benchmark public String maraca() {
    return convert(n);
  }

  private static final char[] magnitudes = {'k', 'M', 'G', 'T', 'P', 'E'}; // enough for long

  public static final String convert(long number) {
    String ret;
    if (number >= 0) {
      ret = "";
    } else if (number <= -9200000000000000000L) {
      return "-9.2E";
    } else {
      ret = "-";
      number = -number;
    }
    if (number < 1000)
      return ret + number;
    for (int i = 0;; i++) {
      if (number < 10000 && number % 1000 >= 100)
        return ret + (number / 1000) + '.' + ((number % 1000) / 100) + magnitudes[i];
      number /= 1000;
      if (number < 1000)
        return ret + number + magnitudes[i];
    }
  }

  private static final String[] suffixes = {"", "k", "M", "G", "T", "P", "E"};

  private static String formatArray(long value) {
    if (value == Long.MIN_VALUE) return formatArray(Long.MIN_VALUE + 1); //to prevent problem on next line
    long positiveValue = Math.abs(value); //works ok because value != MIN_VALUE
    long temp = positiveValue;
    int suffixIndex = 0;
    while ((temp /= 1000) > 0 && suffixIndex < suffixes.length - 1) {
      suffixIndex++;
    }
    double truncatedValue = truncate(positiveValue, suffixIndex);
    String suffix = suffixes[suffixIndex];
    boolean hasDecimal = truncatedValue != (long) truncatedValue;
    String positiveFormatted = hasDecimal ? truncatedValue + suffix : (long) truncatedValue + suffix;
    return (value < 0 ? "-" : "") + positiveFormatted;
  }

  /**
   * @return the number part of the output, truncated as required with the correct number of decimals
   */
  private static double truncate(long positiveValue, int range) {
    if (range == 0) return positiveValue;
    long divideBy = 100; //not 1000 to keep one decimal if needed
    for (int i = 0; i < range - 1; i++) {
      divideBy *= 1000;
    }
    //cast as double after division to avoid loss of precision!!
    double scaled = (positiveValue / divideBy) / 10d; //1000 for k, 1_000_000 for m etc.
    boolean hasDecimal = scaled < 10 && scaled != (long) scaled;
    return hasDecimal ? (long) (scaled * 10) / 10d : (long) scaled;
  }

  private static final NavigableMap<Long, String> suffixesMap = new TreeMap<>();

  static {
    suffixesMap.put(1_000L, "k");
    suffixesMap.put(1_000_000L, "M");
    suffixesMap.put(1_000_000_000L, "G");
    suffixesMap.put(1_000_000_000_000L, "T");
    suffixesMap.put(1_000_000_000_000_000L, "P");
    suffixesMap.put(1_000_000_000_000_000_000L, "E");
  }

  private static String formatMap(long value) {
    //Long.MIN_VALUE == -Long.MIN_VALUE so we need a dirty adjustment
    if (value == Long.MIN_VALUE) return formatMap(Long.MIN_VALUE + 1);
    if (value < 0) return "-" + formatMap(-value);
    if (value < 1000) return value + ""; //deal with easy case

    Map.Entry<Long, String> e = suffixesMap.floorEntry(value);
    Long divideBy = e.getKey();
    String suffix = e.getValue();

    long truncated = value / (divideBy / 10); //the number part of the output times 10
    boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
    return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
  }
}
