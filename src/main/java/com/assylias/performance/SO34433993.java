/*
 * Copyright (C) 2015 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO34433993 {

  @Benchmark public E values() {
    return E.values()[0];
  }

  @Benchmark public E getValues() {
    return E.getValues()[0];
  }
}

enum E {
  A, B, C, D, E, F, G, H, I, J;

  private static final E[] values = values();

  public static E[] getValues() {
    return values;
  }
}
