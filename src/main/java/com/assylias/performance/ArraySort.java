/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import static java.util.concurrent.ForkJoinTask.invokeAll;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;

/**
 *
 */
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
public class ArraySort {

    private static final ForkJoinPool pool = new ForkJoinPool(8);

    public int[] setup() {
        int[] array = new int[8_000_000];
        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < 1_000_000; i++) {
            array[i] = r.nextInt();
        }
        return array;
    }

    @Benchmark
    public int[] sort() {
        int[] array = setup();
        Arrays.sort(array);
        return array;
    }

    @Benchmark
    public int[] parallelSort() {
        int[] array = setup();
        RecursiveAction sort = new PartialParallelSort(array, 0, array.length);

        pool.invoke(sort);
        Arrays.sort(array); //needs a second round to sort the whole array

        return array;
    }

    static class PartialParallelSort extends RecursiveAction {

        private static final int THRESHOLD = 1_000_000;
        private final int[] array;
        private final int from;
        private final int to;

        PartialParallelSort(int[] array, int from, int to) {
            this.array = array;
            this.from = from;
            this.to = to;
        }

        @Override
        protected void compute() {
            if (to - from < THRESHOLD) {
                Arrays.sort(array, from, to);
            } else {
                int middle = (from + to) / 2;
                invokeAll(new PartialParallelSort(array, from, middle),
                          new PartialParallelSort(array, middle + 1, to));
            }
        }
    }

    @Benchmark
    public int[] baseline() {
        int[] array = setup();
        return array;
    }
}
