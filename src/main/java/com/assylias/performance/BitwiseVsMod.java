/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class BitwiseVsMod {

    private Random rand = new Random();
    private long sum = 0;

    @Benchmark
    public boolean modulo() {
        long i = getValue() % 2;
        return isEven(i);
    }

    @Benchmark
    public boolean bitAnd() {
        long i = getValue() & 1;
        return isEven(i);
    }

    @Benchmark
    public boolean bitShift() {
        long i = getValue() << 63;
        return isEven(i);
    }

    @Benchmark
    public boolean baseline() {
        long i = getValue();
        return isEven(i);
    }

    private long getValue() {
        return rand.nextLong();
    }

    private boolean isEven(long i) {
        boolean even = (i == 0);
        if (even) sum++;
        return even;
    }
}
